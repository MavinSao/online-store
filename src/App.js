import Menu from './components/Menu';
import Home from './components/Home';
import { BrowserRouter as Router, Route, Switch, Link, Redirect } from 'react-router-dom'
import Accessories from './components/Accessories';
import Shop from './components/Shop';
import { strings } from './localization/strings'
import React, { Component } from 'react'
import { data } from './data/data'

export default class componentName extends Component {

  constructor() {
    super();
    this.state = {
      data: data,
      search: null
    }
  }
  onSearch = (e) => {
    var value = e.target.value
    this.setState({
      search: value
    }
    )
  }

  changeLanguage = (lang) => {
    strings.setLanguage(lang);
    this.setState({});
  }

  render() {
    return (
      <Router>
        <Menu changeLang={this.changeLanguage} onSearch={this.onSearch} />
        <Switch>
          <Route path='/' exact render={(props) => <Redirect {...props} to="/Home" />} />
          <Route path='/Home' render={(props) => <Home {...props} data={this.state.data} search={this.state.search} />} />
          <Route path='/Accessories' exact component={Accessories} />
          <Route path='/Shop' render={(props) => <Shop {...props} data={this.state.data} />} />
          <Route path='*' render={() => <h1>404 Not Found</h1>} />
        </Switch>
      </Router>
    )
  }
}



