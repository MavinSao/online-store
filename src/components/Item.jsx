import React from "react";
import { faShoppingCart } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
function Item(props) {
  return (
    <div className="col-md-4">
      <div className="card mb-3">
        <div className="card-body">
          <h5 className="card-title">{props.item.brand}</h5>
          <h6 className="card-subtitle text-muted">Support card subtitle</h6>
        </div>
        <img src={props.item.img} alt="img" className="img-fluid" />
        <div className="card-body">
          <p className="card-text">Price : {props.item.price} $</p>
        </div>
        <ul className="list-group list-group-flush">
          <li className="list-group-item">Cras justo odio</li>
          <li className="list-group-item">Dapibus ac facilisis in</li>
          <li className="list-group-item">Vestibulum at eros</li>
        </ul>
        <div className="card-body">
          <button
            type="button"
            className="btn btn-primary"
            onClick={() =>
              props.onBuy(props.category, props.item.id, props.item.brand)
            }
          >
            <FontAwesomeIcon icon={faShoppingCart} /> Buy
          </button>
          <button type="button" className="btn btn-primary mx-2">
            Add
          </button>
        </div>
        <div className="card-footer text-muted">2 days ago</div>
      </div>
    </div>
  );
}

export default Item;
