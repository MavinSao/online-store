import Item2 from "../Item2";
import React from "react";

const searchData = (data, search) => {
  return data.filter((d) => d.brand.includes(search));
};
function MacAir(props) {
  var search = searchData(props.data, props.search);

  let data =
    props.search === null
      ? props.data.map((mac) => (
          <Item2 onBuy={props.onBuy} category={"air"} key={mac.id} item={mac} />
        ))
      : search.map((mac) => (
          <Item2 onBuy={props.onBuy} category={"air"} key={mac.id} item={mac} />
        ));
  return (
    <div className="container">
      <div className="row">
        <h1 className="col">Macbook Air</h1>
      </div>
      <div className="row">{data}</div>
    </div>
  );
}

export default MacAir;
