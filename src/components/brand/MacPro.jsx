import Item from "../Item";
import React from "react";

const searchData = (data, search) => {
  return data.filter((d) => d.brand.includes(search));
};

function MacPro(props) {
  var search = searchData(props.data, props.search);

  let data =
    props.search === null
      ? props.data.map((mac) => (
          <Item onBuy={props.onBuy} category={"pro"} key={mac.id} item={mac} />
        ))
      : search.map((mac) => (
          <Item onBuy={props.onBuy} category={"pro"} key={mac.id} item={mac} />
        ));
  return (
    <div className="container">
      <div className="row">
        <h1 className="col">Macbook Pro</h1>
      </div>
      <div className="row">{data}</div>
    </div>
  );
}

export default MacPro;
