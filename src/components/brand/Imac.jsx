import Item from "../Item";
import React from "react";

const searchData = (data, search) => {
  return data.filter((d) => d.brand.includes(search));
};

function Imac(props) {
  var search = searchData(props.data, props.search);

  let data =
    props.search === null
      ? props.data.map((mac) => (
          <Item onBuy={props.onBuy} category={"imac"} key={mac.id} item={mac} />
        ))
      : search.map((mac) => (
          <Item onBuy={props.onBuy} key={mac.id} item={mac} />
        ));

  return (
    <div>
      <div className="container">
        <div className="row">
          <h1 className="col">IMac</h1>
        </div>
        <div className="row">{data}</div>
      </div>
    </div>
  );
}

export default Imac;
