import React from "react";
import { strings } from "./../localization/strings";
function Accessories() {
  return (
    <div className="container">
      <h1>{strings.accessories}</h1>
    </div>
  );
}

export default Accessories;
