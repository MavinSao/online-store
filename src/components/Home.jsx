import React, { Component } from "react";
import Item from "./Item";
import Imac from "./brand/Imac";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Link,
  Redirect,
} from "react-router-dom";
import MacAir from "./brand/MacAir";
import MacPro from "./brand/MacPro";
import Shop from "./Shop";
export default class componentName extends Component {
  onBuy = (category, id, brand) => {
    console.log("work", id);
    this.props.history.push(
      `/Shop?category=${category}&&brand=${brand}&&id=${id}`
    );
  };

  render() {
    return (
      <Router>
        <div className="container">
          <div className="row my-2">
            <div className="col-md-4">
              <div className="list-group">
                <Link
                  to="/Home"
                  className="list-group-item list-group-item-action"
                >
                  IMac
                </Link>
                <Link
                  to="/Home/MacbookPro"
                  className="list-group-item list-group-item-action"
                >
                  Macbook Pro
                </Link>
                <Link
                  to="/Home/MacbookAir"
                  className="list-group-item list-group-item-action"
                >
                  Macbook Air
                </Link>
              </div>
            </div>
            <div className="col-md-8 card text-white bg-secondary">
              <div className="row no-gutters">
                <div className="col-3">
                  <img
                    src="https://mhcid.washington.edu/wp-content/uploads/2017/06/Apple-logo.png"
                    width="140px"
                    alt="img"
                  />
                </div>
                <div className="col-9">
                  <div className="card-body">
                    <h4 className="card-title">Apple Company</h4>
                    <p className="card-text">
                      Some quick example text to build on the card title and
                      make up the bulk of the card's content.
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <Switch>
          <Route
            path="/Home"
            exact
            render={() => (
              <Imac
                onBuy={this.onBuy}
                data={this.props.data.imac}
                search={this.props.search}
              />
            )}
          />
          <Route
            path="/Home/MacbookPro"
            render={() => (
              <MacPro
                onBuy={this.onBuy}
                data={this.props.data.macPro}
                search={this.props.search}
              />
            )}
          />
          <Route
            path="/Home/MacbookAir"
            render={() => (
              <MacAir
                onBuy={this.onBuy}
                data={this.props.data.macAir}
                search={this.props.search}
              />
            )}
          />
          <Route path="/Shop" handler={Shop} />
          <Route path="*" render={() => <h1>404 Not Found</h1>} />
        </Switch>
      </Router>
    );
  }
}
