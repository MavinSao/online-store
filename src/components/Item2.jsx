import React from "react";
import { faShoppingCart } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
function Item2(props) {
  return (
    <div className="col-md-12">
      <div className="card mb-3">
        <div className="row no-gutters">
          <div className="col-4">
            <img src={props.item.img} alt="img" className="img-fluid mt-1" />
          </div>
          <div className="col-8">
            <div className="card-body">
              <h5 className="card-title">{props.item.brand}</h5>
              <h6 className="card-subtitle text-muted">
                Support card subtitle
              </h6>
            </div>
            <div className="card-body">
              <p className="card-text">Price : {props.item.price} $</p>
            </div>
            <div className="card-body">
              <button
                type="button"
                className="btn btn-primary"
                onClick={() =>
                  props.onBuy(props.category, props.item.id, props.item.brand)
                }
              >
                <FontAwesomeIcon icon={faShoppingCart} /> Buy
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Item2;
