import React from "react";
import { Table } from "react-bootstrap";
import queryString from "query-string";
import { Link } from "react-router-dom";
const getItem = (data, path) => {
  switch (path.category) {
    case "imac":
      return data.imac.find((d) => d.brand === path.brand);
    case "pro":
      return data.macPro.find((d) => d.brand === path.brand);
    case "air":
      return data.macAir.find((d) => d.brand === path.brand);
    default:
      return;
  }
};

function Shop(props) {
  var path = queryString.parse(props.location.search);
  var product = getItem(props.data, path);

  return (
    <div className="container">
      <h1 className="my-2">Shop</h1>
      <form>
        <div className="form-group row">
          <div className="col-md-12">
            <label htmfor="email" className=" col-form-label">
              Email
            </label>
            <div className="form-group">
              <input
                className="form-control form-control-sm"
                type="email"
                placeholder="example@gmail.com"
                id="inputSmall"
              />
            </div>
            {product !== undefined ? (
              <Table striped bordered variant="dark">
                <thead>
                  <tr>
                    <th colSpan={2}>
                      <div className="row">
                        <img
                          src={product.img}
                          alt="img"
                          className="img-fluid col-md-3"
                        />
                        <div className="col-md-9">
                          <h1>Summary</h1>
                        </div>
                      </div>
                    </th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>ID</td>
                    <td>{product.id}</td>
                  </tr>
                  <tr>
                    <td>Brand</td>
                    <td>{product.brand}</td>
                  </tr>
                  <tr>
                    <td>Shiping Fee</td>
                    <td>0.00$</td>
                  </tr>
                  <tr>
                    <td>Price</td>
                    <td>{product.price} $</td>
                  </tr>
                </tbody>
              </Table>
            ) : (
              <div className="row">
                <div className="col-md-12">
                  <Link to="/">
                    <h1 className="text-center">Check Our Product</h1>
                  </Link>
                </div>
              </div>
            )}
            <img
              src="https://wpchats.com/wp-content/uploads/2015/05/Payment-gateway-of-WordPress.png"
              alt="img"
              className="img-fluid"
            />
          </div>
        </div>
      </form>
    </div>
  );
}

export default Shop;
