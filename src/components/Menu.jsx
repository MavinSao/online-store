import React from "react";
// get our fontawesome imports
import { faCode, faStore } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  Navbar,
  Nav,
  NavDropdown,
  Form,
  FormControl,
  Button,
  Container,
} from "react-bootstrap";
import { Link } from "react-router-dom";
import { strings } from "./../localization/strings";

function Menu(props) {
  return (
    <div>
      <Navbar bg="dark" variant="dark" expand="lg">
        <Container>
          <Navbar.Brand as={Link} to="/">
            <FontAwesomeIcon icon={faCode} /> XSTORE
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="mr-auto">
              <Nav.Link as={Link} to="/Home">
                {strings.home}
              </Nav.Link>
              <Nav.Link as={Link} to="/Accessories">
                {strings.accessories}
              </Nav.Link>
              <Nav.Link as={Link} to="/Shop">
                {strings.shop}
              </Nav.Link>
              <NavDropdown title={strings.language} id="basic-nav-dropdown">
                <NavDropdown.Item onClick={() => props.changeLang("en")}>
                  English
                </NavDropdown.Item>
                <NavDropdown.Item onClick={() => props.changeLang("kh")}>
                  Khmer
                </NavDropdown.Item>
              </NavDropdown>
            </Nav>
            <Form inline>
              <FormControl
                type="text"
                placeholder="Search"
                className="mr-sm-2"
                name="search"
                onChange={(e) => props.onSearch(e)}
              />
              <Button variant="outline-light">Search</Button>
            </Form>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </div>
  );
}

export default Menu;
