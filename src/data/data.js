export const data = {
    imac: [
        {
            id: "1",
            brand: "IMac 2018",
            img:
                "https://hardwarevillagengr.com/wp-content/uploads/2019/12/Apple-iMac-27-Inch-All-In-One-Desktop-Computer-Intel-Core-i5-3.4GHz-Processor-8GB-RAM.jpg",
            price: "250",
        },
        {
            id: "2",
            brand: "IMac 2019",
            img:
                "https://hardwarevillagengr.com/wp-content/uploads/2019/12/Apple-iMac-27-Inch-All-In-One-Desktop-Computer-Intel-Core-i5-3.4GHz-Processor-8GB-RAM.jpg",
            price: "300",
        },
        {
            id: "3",
            brand: "IMac 2020",
            img:
                "https://hardwarevillagengr.com/wp-content/uploads/2019/12/Apple-iMac-27-Inch-All-In-One-Desktop-Computer-Intel-Core-i5-3.4GHz-Processor-8GB-RAM.jpg",
            price: "350",
        },
    ],
    macAir: [
        {
            id: "1",
            brand: "Macbook Air 2018",
            img:
                "https://i.pcmag.com/imagery/reviews/05LXWfM1KdbGDpwYgreAMVz-25..v_1569479597.jpg",
            price: "250",
        },
        {
            id: "2",
            brand: "Macbook Air 2019",
            img:
                "https://i.pcmag.com/imagery/reviews/05LXWfM1KdbGDpwYgreAMVz-25..v_1569479597.jpg",
            price: "300",
        },
        {
            id: "3",
            brand: "Macbook Air 2020",
            img:
                "https://i.pcmag.com/imagery/reviews/05LXWfM1KdbGDpwYgreAMVz-25..v_1569479597.jpg",
            price: "350",
        },
    ],
    macPro: [
        {
            id: "1",
            brand: "Macbook Pro 2018",
            img:
                "https://azcd.harveynorman.com.au/media/catalog/product/a/n/anz_macbook_pro_16in_spacegrey_pdp_1_rgb_1.jpg",
            price: "250",
        },
        {
            id: "2",
            brand: "Macbook Pro 2019",
            img:
                "https://azcd.harveynorman.com.au/media/catalog/product/a/n/anz_macbook_pro_16in_spacegrey_pdp_1_rgb_1.jpg",
            price: "300",
        },
        {
            id: "3",
            brand: "Macbook Pro 2020",
            img:
                "https://azcd.harveynorman.com.au/media/catalog/product/a/n/anz_macbook_pro_16in_spacegrey_pdp_1_rgb_1.jpg",
            price: "350",
        },
    ],
};
