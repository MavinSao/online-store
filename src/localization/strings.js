import LocalizedString from 'localized-strings';
export const strings = new LocalizedString({
    en: {
        home: "Home",
        accessories: "Accessories",
        shop: "Shop",
        language: "Language",

    },
    kh: {
        home: "ទំព័រដើម",
        accessories: "គ្រឿង",
        shop: "ទំនិញ",
        language: "ភាសារ",
    },
})